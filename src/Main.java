
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Scanner;
import java.util.function.DoubleConsumer;

public class Main {
/*RGB
 CMY
 CMYK(K-addition of black)
 HSV(Hue, Saturation, Value)(Тон, Насыщенность, Значение)
 HSL (hue, saturation, lightness)[0,360] [0,1] [0,1]
 XYZ
 */

    public static double[] readColor(int colorQuantity) {//Done :)
        double[] color = new double[colorQuantity];
        Scanner sc = new Scanner(System.in);//TODO: подобрать  pattern
        for (int i = 0; i < color.length; i++) {
            color[i] = sc.nextDouble();
        }
      //  sc.close();
        return color;
    }

    public static double[] rgbTocmy(double[] rgb) {//Done :)
        //System.out.println("RGB to CMY");
        double[] cmy = new double[]{1d - (rgb[0]), 1d - (rgb[1]), 1d - (rgb[2])};
        return cmy;
    }


    public static double[] rgbTocmyk(double[] rgb) {//Done :)
        double k=1-Math.max(rgb[0],Math.max(rgb[1],rgb[2]));
        return new double[]{(1-rgb[0]-k)/(1-k),(1-rgb[1]-k)/(1-k),(1-rgb[2]-k)/(1-k),k};
    }

    public static double[] rgbTohsv(double[] rgb) {//Done :)
        double max = Math.max(rgb[0], Math.max(rgb[1], rgb[2]));
        double min = Math.min(rgb[0], Math.min(rgb[1], rgb[2]));
        double h, s;
        //hue
        if (Double.compare(max,min) == 0) h = 0;//TODO: else после этого if
            else if (Double.compare(max,rgb[0]) == 0) {
            if (Double.compare(rgb[1],rgb[2]) >= 0)
                h = 60 * (rgb[1] - rgb[2]) / (max - min);
            else h = 60 * (rgb[1] - rgb[2]) / (max - min) + 360;
        } else if (Double.compare(max,rgb[1]) == 0)
            h = 60 * (rgb[2] - rgb[0]) / (max - min) + 120;
        else if (Double.compare(max,rgb[2]) == 0)
            h = 60 * (rgb[0] - rgb[1]) / (max - min) + 240;
        else
            throw new IllegalArgumentException("Something wrong with a variable max, because all conditions before throwing this exception are false");
        //saturation
        s = Double.compare(max,0) == 0 ? 0 : (1 - min / max);
        return new double[]{h, s, max};
    }

    public static double[] rgbTohsl(double[] rgb) {//Done :)
        double max = Math.max(rgb[0], Math.max(rgb[1], rgb[2]));
        double min = Math.min(rgb[0], Math.min(rgb[1], rgb[2]));
        double h, s, l;
        //hue
        if (Double.compare(max,min)==0) h = 0;
        else if (Double.compare(max,rgb[0]) == 0) {
            if (Double.compare(rgb[1],rgb[2]) >= 0)
                h =60 * (rgb[1] - rgb[2]) / (max - min);
            else h = 60 * (rgb[1] - rgb[2]) / (max - min) + 360;
        } else if (Double.compare(max,rgb[1]) == 0)
            h = 60 * (rgb[2] - rgb[0]) / (max - min) + 120;
        else if (Double.compare(max,rgb[2]) == 0)
            h = 60 * (rgb[0] - rgb[1]) / (max - min) + 240;
        else
            throw new IllegalArgumentException("Something wrong with a variable max, because all conditions before throwing this exception are false");
        //lightness
        l = 0.5*(max+min);
        //saturation
        if (Double.compare(l,0) == 0 || Double.compare(max,min) == 0)
            s = 0;
        else if (Double.compare(l,0) > 0 && Double.compare(l,0.5) <= 0)
            s = (max-min)/(2*l);
        else if (Double.compare(l,0.5) > 0 && Double.compare(l,1) < 0)
            s=(max-min)/(2-2*l);
        else
            throw new IllegalArgumentException("Something wrong with a variable max, because all conditions before throwing this exception are false");


        return new double[]{h, s, l};
    }

    public static double[] rgbToxyz(double[] rgb) {
        double[] xyz = new double[3];
        xyz[0] = (0.49d * (double) rgb[0] + 0.31d * (double) rgb[1] + 0.20d * (double) rgb[2]) / (0.17697d);
        xyz[1] = (0.17697d * rgb[0] + 0.81240d * rgb[1] + 0.01063d * rgb[2]) / (0.17697d);
        xyz[2] = (0.00d * rgb[0] + 0.01d * rgb[1] + 0.99d * rgb[2]) / (0.17697d);
        return xyz;
    }


    public static double[] cmyTorgb(double[] cmy) {//Done :)
        System.out.println("RGB to XYZ");
        double[] result = new double[]{(-cmy[0] + 1), (-cmy[1] + 1), (-cmy[2] + 1)};
        System.out.println(Arrays.toString(result));
        return result;
    }

    

    public static double[] cmykTorgb(double[] cmyk) {//Done :)
        return new double[]{(1-cmyk[0])*(1-cmyk[3]),(1-cmyk[1])*(1-cmyk[3]),(1-cmyk[2])*(1-cmyk[3])};
    }

    public static double[] hsvTorgb(double[] hsv) {//Done :)
        double[] n_hsv = new double[]{hsv[0], hsv[1] * 100, hsv[2] * 100};
        int hi = (int) (n_hsv[0] / 60) % 6;
        double vmin = (100 - n_hsv[1]) * n_hsv[2] / 100;
        double a = (n_hsv[2] - vmin) * ((int) (n_hsv[0]) % 60) / 60d;
        double vinc = (vmin + a) / 100;
        double vdec = (n_hsv[2] - a) / 100;
        vmin /= 100;
        n_hsv[2] /= 100;
        switch (hi) {
            case 0:
                return new double[]{n_hsv[2], vinc, vmin};
            case 1:
                return new double[]{vdec, n_hsv[2], vmin};
            case 2:
                return new double[]{vmin, n_hsv[2], vinc};
            case 3:
                return new double[]{vmin, vdec, n_hsv[2]};
            case 4:
                return new double[]{vinc, vmin, n_hsv[2]};
            case 5:
                return new double[]{n_hsv[0], vmin, vdec};
            default:
                throw new IllegalArgumentException("Problems with a value of a variable hi");
        }
    }


    public static double[] hslTorgb(double[] hsl) {//Done :)
        //    System.out.println("HSL To RGB");
        double q, p;
        double[] t = new double[3];
        double[] result = new double[3];
        if (hsl[2] < 0.5)
            q = hsl[2] * (1 + hsl[1]);
        else q = hsl[2] + hsl[1] - (hsl[2] * hsl[1]);
        p = 2 * hsl[2] - q;
        double hk = hsl[0] / 360d;
        t[0] = hk + 1 / 3d;
        t[1] = hk;
        t[2] = hk - 1 / 3d;
        for (int i = 0; i < result.length; i++)
            if (t[i] < 0) t[i] += 1;
            else if (t[i] > 1) t[i] -= 1;

        for (int i = 0; i < result.length; i++)
            if (t[i] < 1 / 6d) result[i] = p + ((q - p) * 6 * t[i]);
            else if (t[i] >= 1 / 6d && t[i] < 1 / 2d) result[i] = q;
            else if (t[i] >= 1 / 2d && t[i] < 2 / 3d) result[i] = p + ((q - p) * (2 / 3d - t[i]) * 6);
            else result[i] = p;

        return result;
    }

    public static double[] xyzTorgb(double[] xyz) {
        System.out.println("XYZ to RGB");
        double[] crnt_rgb = new double[3];
        crnt_rgb[0] = (2.364613d * xyz[0] - 0.89654d * xyz[1] - 0.468073d * xyz[2]) * 0.17697d;
        crnt_rgb[1] = (-0.515166d * xyz[0] + 1.426408d * xyz[1] + 0.088758d * xyz[2]) * 0.17697d;
        crnt_rgb[2] = (0.005203d * xyz[0] - 0.014408 * xyz[1] + 1.009205d * xyz[2]) * 0.17697d;
        System.out.println(Arrays.toString(crnt_rgb));

        return crnt_rgb;

    }

    public static double[] cmyTocmyk(double[] cmy) {//Done :)
        double min = Math.min(cmy[0], Math.min(cmy[1], Math.min(cmy[2], 1)));
        double[] cmyk = new double[]{(cmy[0] - min) / (1 - min), (cmy[1] - min) / (1 - min), (cmy[2] - min) / (1 - min), min};
        return cmyk;
    }
    public static double[] cmyTohsv(double[] cmy){
        return rgbTohsv(cmyTorgb(cmy));
    }
    public static double[] cmyTohsl(double[] cmy){
        return rgbTohsl(cmyTorgb(cmy));
    }

    public static double[] cmyToxyz(double[] cmy){
        return rgbToxyz(cmyTorgb(cmy));
    }
    public static double[] cmykTocmy(double[] cmyk){
        return rgbTocmy(cmykTorgb(cmyk));
    }
    public static double[] cmykTohsv(double [] cmyk){
        return rgbTohsv(cmykTorgb(cmyk));
    }
    public static double[] cmykTohsl(double[] cmyk){
        return rgbTohsl(cmykTorgb(cmyk));
    }
    public static double[] cmykToxyz(double [] cmyk){
        return rgbToxyz(cmykTorgb(cmyk));
    }
    public static double[] hsvTocmy(double[] hsv){
        return rgbTocmy(hsvTorgb(hsv));
    }
    public static double[] hsvTocmyk(double[] hsv){
        return rgbTocmyk(hsvTorgb(hsv));
    }
    public static double[] hsvTohsl(double[] hsv){
        return rgbTohsl(hsvTorgb(hsv));
    }
    public static double[] hsvToxyz(double[] hsv){
        return rgbToxyz(hsvTorgb(hsv));
    }
    public static double[] hslTocmy(double[] hsl){
        return rgbTocmy(hslTorgb(hsl));
    }
    public static double[] hslTocmyk(double[] hsl){
        return rgbTocmyk(hslTorgb(hsl));
    }
    public static double[] hslTohsv(double[] hsl){
        return rgbTohsv(hslTorgb(hsl));
    }
    public static double[] hslToxyz(double[] hsl){
        return rgbToxyz(hslTorgb(hsl));
    }
    public static double[] xyzTocmy(double[] xyz){
        return rgbTocmy(xyzTorgb(xyz));
    }
    public static double[] xyzTocmyk(double[] xyz){
        return rgbTocmyk(xyzTorgb(xyz));
    }
    public static double[] xyzTohsv(double[] xyz){
        return rgbTohsv(xyzTorgb(xyz));
    }
    public static double[] xyzTohsl(double [] xyz){
        return rgbTohsl(xyzTorgb(xyz));
    }

    public static void main(String[] args) {
       System.out.println("Color");
       double[] color=readColor(3);
       System.out.format("Your input:%s\n",Arrays.toString(color));
        System.out.println("Which color was input?");
        Scanner sc=new Scanner(System.in);
       String  name=sc.nextLine();
       sc.close();
        switch(name){
            case "rgb":
            System.out.println(Arrays.toString(rgbTocmy(color)));
            System.out.println(Arrays.toString(rgbTocmyk(color)));
            System.out.println(Arrays.toString(rgbTohsv(color)));
            System.out.println(Arrays.toString(rgbTohsl(color)));
            System.out.println(Arrays.toString(rgbToxyz(color)));
            break;
            case "cmy":
            System.out.println(Arrays.toString(cmyTorgb(color)));
            System.out.println(Arrays.toString(rgbTocmy(color)));
            System.out.println(Arrays.toString(rgbTocmyk(color)));
            System.out.println(Arrays.toString(cmyTohsv(color)));
            System.out.println(Arrays.toString(cmyTohsl(color)));
            System.out.println(Arrays.toString(hsvToxyz(color)));
            break;
            case "cmyk":
            System.out.println(Arrays.toString(cmykTorgb(color)));
            System.out.println(Arrays.toString(cmykTocmy(color)));
            System.out.println(Arrays.toString(cmykTohsv(color)));
            System.out.println(Arrays.toString(cmykTohsl(color)));
            System.out.println(Arrays.toString(cmykToxyz(color)));
            break;
            case "hsv":
            System.out.println(Arrays.toString(hsvTorgb(color)));
            System.out.println(Arrays.toString(hsvTocmy(color)));
            System.out.println(Arrays.toString(hsvTocmyk(color)));
            System.out.println(Arrays.toString(hsvTohsl(color)));
            System.out.println(Arrays.toString(hsvToxyz(color)));
            break;
            case "hsl":
            System.out.println(Arrays.toString(hslTorgb(color)));
            System.out.println(Arrays.toString(hslTocmy(color)));
            System.out.println(Arrays.toString(hslTocmyk(color)));
            System.out.println(Arrays.toString(hslTohsv(color)));
            System.out.println(Arrays.toString(hslToxyz(color)));
            break;
            case "xyz":
            System.out.println(Arrays.toString(xyzTorgb(color)));
            System.out.println(Arrays.toString(xyzTocmy(color)));
            System.out.println(Arrays.toString(xyzTocmyk(color)));
            System.out.println(Arrays.toString(xyzTohsv(color)));
            System.out.println(Arrays.toString(xyzTohsl(color)));
            break;
            default:
            System.out.println(":(");
            break;
        }
    }
}
