
import javafx.application.*;
import javafx.scene.*;
import javafx.scene.paint.Color;
import javafx.stage.*;
import javafx.scene.layout.*;
import javafx.scene.control.*;
import javafx.scene.image.*;
import javafx.scene.text.Text;

public class FXMain extends Application{
    public Group group;
public static void main(String[] args){
launch(args);
}
@Override
public void start(Stage stage) {
     
     //установка надписи
    //Text text = new Text("Hello from JavaFX!");
    //text.setLayoutY(80);    // установка положения надписи по оси Y
    //text.setLayoutX(100);   // установка положения надписи по оси X
    WritableImage wImage=drawLine(1,1,1,1,Color.BLUE);

    group = new Group(new ImageView(wImage));
    Scene scene = new Scene(group,500,500);
    stage.setScene(scene);
    stage.setTitle("First Application");
    stage.setWidth(300);
    stage.setHeight(250);
    stage.show();
}
/**
 * Draws a line using a DDA algorithm
 * @param wr {@link PixelWriter} for drawing of pixels.
 */
public WritableImage drawLine(int x1, int y1, int x2, int y2,Color color){
    int x,y,dx,dy;
    WritableImage image=new WritableImage(Math.abs(x1-x2),Math.abs(y1-y2));
    dx=1;
    dy=Math.abs(y2-y1)/Math.abs(x2-x1);
    x=x1;
    y=y1;
    while(x<=x2){
    image.getPixelWriter().setColor(x,y,c);
        x+=dx;
        y+=dy;
    }
    //image.setLayoutX(20);
    //image.setLayoutY(20);
    return image;
    }
}